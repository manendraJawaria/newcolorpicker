//
//  ColorPickerViewController.h
//  ColorPicker Project
//
//  Created by Iftekhar on 14/01/16.
//  Copyright © 2016 iftekhar. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class IZValueSelectorView;
//@class HRColorPickerView;
@protocol ColorPickerViewControllerDelegate

-(void)setSelectedColor:(UIColor*)color;
@end

@interface ColorPickerViewController : UIViewController

@property (strong,nonatomic)  UIColor *getColor;
@property (weak,nonatomic)id<ColorPickerViewControllerDelegate>pickerDelegate;

@end
