//
//  AppDelegate.h
//  ColorPicker Project
//
//  Created by Iftekhar on 14/01/16.
//  Copyright © 2016 iftekhar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

