//
//  RGBValuesViewController.m
//  ColorPicker Project
//
//  Created by Iftekhar on 15/01/16.
//  Copyright © 2016 iftekhar. All rights reserved.
//

#import "RGBValuesViewController.h"
#import "IZValueSelectorView.h"

@interface RGBValuesViewController ()<IZValueSelectorViewDataSource,IZValueSelectorViewDelegate>
{

    NSUInteger redColor, greenColor, blueColor,getRedValue,getGreenValue,getBlueValue;
    
}
@property (strong, nonatomic) IBOutlet UIView *labelView;
@property (strong, nonatomic) IBOutlet UILabel *hexLabel;
@property (strong, nonatomic) IBOutlet UILabel *rgbLabel;
@property (strong, nonatomic) IBOutlet UIView *colorView;
@property (strong, nonatomic) IBOutlet IZValueSelectorView *selectorHorizontol1;
@property (strong, nonatomic) IBOutlet IZValueSelectorView *selectorHorizontol2;
@property (strong, nonatomic) IBOutlet IZValueSelectorView *selectorHorizontol3;
@property (strong, nonatomic) IBOutlet UILabel *redColorLabel;
@property (strong, nonatomic) IBOutlet UILabel *greenColorLabel;
@property (strong, nonatomic) IBOutlet UILabel *blueColorLabel;
@property (strong, nonatomic) IBOutlet UIButton *redMinus;
@property (strong, nonatomic) IBOutlet UIButton *redPlus;
@property (strong, nonatomic) IBOutlet UIButton *greenPlus;
@property (strong, nonatomic) IBOutlet UIButton *greenMinus;
@property (strong, nonatomic) IBOutlet UIButton *blueMinus;
@property (strong, nonatomic) IBOutlet UIButton *bluePlus;

@end

@implementation RGBValuesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.redMinus.enabled=NO;
//    self.greenMinus.enabled=NO;
//    self.blueMinus.enabled=NO;
    
    self.colorView.layer.borderColor = [UIColor blackColor].CGColor;
    self.colorView.layer.borderWidth = 1.0f;
    
    self.hexLabel.layer.cornerRadius=6.0;
    self.hexLabel.clipsToBounds=YES;
    self.rgbLabel.layer.cornerRadius=6.0;
    self.rgbLabel.clipsToBounds=YES;
    
//    self.redMinus.layer.borderWidth=1.0;
//    self.redMinus.layer.cornerRadius=10;
    self.redMinus.layer.borderColor=[UIColor redColor].CGColor;
    self.redPlus.layer.borderColor=[UIColor redColor].CGColor;
    
    
    self.redColorLabel.layer.masksToBounds=YES;
    self.greenColorLabel.layer.masksToBounds=YES;
    self.blueColorLabel.layer.masksToBounds=YES;
    self.labelView.layer.masksToBounds=YES;

    
    self.selectorHorizontol1.dataSource = self;
    self.selectorHorizontol1.delegate = self;
    self.selectorHorizontol1.shouldBeTransparent = YES;
    self.selectorHorizontol1.horizontalScrolling = YES;
    
    //You can toggle Debug mode on selectors to see the layout
    self.selectorHorizontol1.debugEnabled = NO;
    
    self.selectorHorizontol2.dataSource = self;
    self.selectorHorizontol2.delegate = self;
    self.selectorHorizontol2.shouldBeTransparent = YES;
    self.selectorHorizontol2.horizontalScrolling = YES;
    
    //You can toggle Debug mode on selectors to see the layout
    self.selectorHorizontol2.debugEnabled = NO;
    
    self.selectorHorizontol3.dataSource = self;
    self.selectorHorizontol3.delegate = self;
    self.selectorHorizontol3.shouldBeTransparent = YES;
    self.selectorHorizontol3.horizontalScrolling = YES;
    
    //You can toggle Debug mode on selectors to see the layout
    self.selectorHorizontol3.debugEnabled = NO;

    self.redColorLabel.layer.cornerRadius=6.0;
    self.redColorLabel.clipsToBounds=YES;
    self.greenColorLabel.layer.cornerRadius=6.0;
    self.greenColorLabel.clipsToBounds=YES;
    self.blueColorLabel.layer.cornerRadius=6.0;
    self.blueColorLabel.clipsToBounds=YES;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.labelView.layer.cornerRadius=self.labelView.frame.size.height/2;
    self.redColorLabel.layer.cornerRadius=self.redColorLabel.frame.size.height/2;
    self.greenColorLabel.layer.cornerRadius=self.greenColorLabel.frame.size.height/2;
    self.blueColorLabel.layer.cornerRadius=self.blueColorLabel.frame.size.height/2;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _colorView.backgroundColor=_getColor;
    self.hexLabel.text=[self hexStringFromColor:_getColor];
    self.rgbLabel.text=[self rgbValueWithHexString:self.hexLabel.text];
    
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.selectorHorizontol1 reloadData];
    [self.selectorHorizontol1 selectRowAtIndex:getRedValue];
    [self.selectorHorizontol2 reloadData];
    [self.selectorHorizontol2 selectRowAtIndex:getGreenValue];
    [self.selectorHorizontol3 reloadData];
    [self.selectorHorizontol3 selectRowAtIndex:getBlueValue];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma IZValueSelector dataSource
- (NSInteger)numberOfRowsInSelector:(IZValueSelectorView *)valueSelector {
    return 256;
}
- (CGFloat)rowHeightInSelector:(IZValueSelectorView *)valueSelector {
    return 30.0;
}
- (CGFloat)rowWidthInSelector:(IZValueSelectorView *)valueSelector {
    return 5.0;
}
- (UIView *)selector:(IZValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index
{
    return [self selector:valueSelector viewForRowAtIndex:index selected:NO];
}

- (UIView *)selector:(IZValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index selected:(BOOL)selected
{
    UIView * label = nil;
    
    if ((index%10)==0)
    {
        label = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2, 30)];
    }
    else if ((index%5)==0)
    {
        label = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 2, 20)];
    }
    else
    {
        label = [[UIView alloc] initWithFrame:CGRectMake(0, 15, 1, 15)];
    }
    if(selected)
    {
        label.backgroundColor=[UIColor blackColor];
        
        if(valueSelector == self.selectorHorizontol1)
        {
           
            if (index == 255) {
                self.redPlus.enabled=NO;
                self.redMinus.enabled=YES;
            }
            else if (index==0) {
                self.redMinus.enabled=NO;
                self.redPlus.enabled=YES;
            }else
            {
                self.redPlus.enabled=YES;
                self.redMinus.enabled=YES;
            }
            
            
        }
        else if(valueSelector == self.selectorHorizontol2)
        {
            if (index == 255) {
                self.greenPlus.enabled=NO;
                self.greenMinus.enabled=YES;
            }
            else if (index==0) {
                self.greenMinus.enabled=NO;
                self.greenPlus.enabled=YES;
            }else
            {
                self.greenPlus.enabled=YES;
                self.greenMinus.enabled=YES;
            }
            
        }
        else
        {
            if (index == 255) {
                self.bluePlus.enabled=NO;
                self.blueMinus.enabled=YES;
            }
            else if (index==0) {
                self.blueMinus.enabled=NO;
                self.bluePlus.enabled=YES;
            }else
            {
                self.bluePlus.enabled=YES;
                self.blueMinus.enabled=YES;
            }
            
        }

    }
    else
    {
        if (valueSelector == self.selectorHorizontol1)
        {
            label.backgroundColor = self.redColorLabel.tintColor;
        }
        else if(valueSelector == self.selectorHorizontol2)
        {
            label.backgroundColor = self.greenColorLabel.tintColor;
        }
        else
        {
            label.backgroundColor = self.blueColorLabel.tintColor;
        }
    }
    

    return label;
}

- (CGRect)rectForSelectionInSelector:(IZValueSelectorView *)valueSelector
{
    return CGRectMake(valueSelector.frame.size.width/2, valueSelector.frame.size.height/2, 1, 1);
}

#pragma IZValueSelector delegate
- (void)selector:(IZValueSelectorView *)valueSelector didSelectRowAtIndex:(NSInteger)index {
    NSLog(@"RGB Selected index %ld",(long)index);
    
    if(valueSelector == self.selectorHorizontol1)
    {
        redColor=(long)index;
        
        if (index == 255) {
            self.redPlus.enabled=NO;
            self.redMinus.enabled=YES;
        }
        else if (index==0) {
            self.redMinus.enabled=NO;
            self.redPlus.enabled=YES;
        }else
        {
            self.redPlus.enabled=YES;
            self.redMinus.enabled=YES;
        }
        
        
    }
    else if(valueSelector == self.selectorHorizontol2)
    {
        greenColor=(long)index;
        if (index == 255) {
            self.greenPlus.enabled=NO;
            self.greenMinus.enabled=YES;
        }
        else if (index==0) {
            self.greenMinus.enabled=NO;
            self.greenPlus.enabled=YES;
        }else
        {
            self.greenPlus.enabled=YES;
            self.greenMinus.enabled=YES;
        }

    }
    else
    {
        blueColor=(long)index;
        if (index == 255) {
            self.bluePlus.enabled=NO;
            self.blueMinus.enabled=YES;
        }
        else if (index==0) {
            self.blueMinus.enabled=NO;
             self.bluePlus.enabled=YES;
        }else
        {
            self.bluePlus.enabled=YES;
            self.blueMinus.enabled=YES;
        }

    }
    UIColor *color=[UIColor colorWithRed:redColor/255.0f green:greenColor/255.0f blue:blueColor/255.0f alpha:1.0f];
    _colorView.backgroundColor=color;
    self.color=color;
    self.redColorLabel.text=[NSString stringWithFormat:@"%ld",redColor];
    self.greenColorLabel.text=[NSString stringWithFormat:@"%ld",greenColor];
    self.blueColorLabel.text=[NSString stringWithFormat:@"%ld",blueColor];
    
    self.rgbLabel.text=[NSString stringWithFormat:@"(%ld,%ld,%ld)",redColor,greenColor,blueColor];
    NSString *colorString=[self hexStringFromColor:color];
    self.hexLabel.text=colorString;
    
    
}
//Return RGB value string
-(NSString* )rgbValueWithHexString:(NSString*)string
{
    NSString *stringColor = string;
    NSUInteger red, green, blue;
    sscanf([stringColor UTF8String], "#%2lX%2lX%2lX", &red, &green, &blue);
    getRedValue=red;
    getGreenValue=green;
    getBlueValue=blue;
    return [NSString stringWithFormat:@"%lu,%lu,%lu",(unsigned long)red,(unsigned long)green,(unsigned long)blue];
}
- (NSString *)hexStringFromColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    
    return [NSString stringWithFormat:@"#%02lX%02lX%02lX",
            lroundf(r * 255),
            lroundf(g * 255),
            lroundf(b * 255)];
}
- (IBAction)redPlusAction:(id)sender
{
    
        [self.selectorHorizontol1 reloadData];
        [self.selectorHorizontol1 selectRowAtIndex:redColor+1];
}

- (IBAction)redMinusAction:(id)sender
{
    [self.selectorHorizontol1 reloadData];
    [self.selectorHorizontol1 selectRowAtIndex:redColor-1];
    
}
- (IBAction)greenPlusAction:(id)sender
{
    
    [self.selectorHorizontol2 reloadData];
    [self.selectorHorizontol2 selectRowAtIndex:greenColor+1];
    
}

- (IBAction)greenMinusAction:(id)sender
{
    [self.selectorHorizontol2 reloadData];
    [self.selectorHorizontol2 selectRowAtIndex:greenColor-1];
    
}
- (IBAction)bluePlusAction:(id)sender
{
    
    [self.selectorHorizontol3 reloadData];
    [self.selectorHorizontol3 selectRowAtIndex:blueColor+1];
    
}

- (IBAction)blueMinusAction:(id)sender
{
    [self.selectorHorizontol3 reloadData];
    [self.selectorHorizontol3 selectRowAtIndex:blueColor-1];
    
}
- (IBAction)buttonDoneAction:(id)sender
{
    if (self.delegate) {
        [self.delegate setSelectedColor:self.color];
    }
    [self.navigationController popViewControllerAnimated:YES ];
}


@end
