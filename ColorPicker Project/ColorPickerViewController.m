//
//  ColorPickerViewController.m
//  ColorPicker Project
//
//  Created by Iftekhar on 14/01/16.
//  Copyright © 2016 iftekhar. All rights reserved.
//

#import "ColorPickerViewController.h"
#import "RGBValuesViewController.h"
#import "HexValuesViewController.h"
#import "HRColorPickerView.h"
#import "IZValueSelectorView.h"



@interface ColorPickerViewController ()<IZValueSelectorViewDataSource,IZValueSelectorViewDelegate,UIGestureRecognizerDelegate>
{
    UIColor *lightColor1,*lightColor2,*lightColor3,*lightColor4,*mainColor,*darkColor1,*darkColor2,*darkColor3,*darkColor4,*darkColor5,*finalColor,*sendColor,*pickColor;
    
    UIView *hexView,*rgbView;
    UILabel *hexLable2,*rgbLable2;
    CAShapeLayer *_colorDotLayer;
}
@property (strong, nonatomic) IBOutlet UIView *valueSelecterView;
@property (strong, nonatomic) IBOutlet UIView *hexRgbColorView;
@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UIView *tringleView;

@end

@implementation ColorPickerViewController
{
    
    HRColorPickerView *colorPickerView;
    IZValueSelectorView *izValueSelecter;
}
@synthesize pickerDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
        //ValueSeleter
    {
        izValueSelecter =[[IZValueSelectorView alloc] init];
        izValueSelecter.backgroundColor=[UIColor clearColor];
        [self.valueSelecterView addSubview:izValueSelecter];
    }
    // Color picker
    {
        colorPickerView = [[HRColorPickerView alloc] init];
        [colorPickerView addTarget:self
                            action:@selector(colorWasChanged:)
                  forControlEvents:UIControlEventValueChanged];
        
        [self.pickerView addSubview:colorPickerView];
    }
    //Buttons with views
    {
        hexView=[[UIView alloc]initWithFrame:CGRectMake(10, 15, 145, 70)];
        hexView.backgroundColor=[UIColor grayColor];
        hexView.layer.masksToBounds=YES;
        hexView.layer.cornerRadius=hexView.frame.size.height/2;
        
        UIView *hexValueBackgroundView=[[UIView alloc]initWithFrame:CGRectMake(12, 10, 120, 50)];
        hexValueBackgroundView.backgroundColor=[[UIColor grayColor] colorWithAlphaComponent:0.5];
        hexValueBackgroundView.layer.masksToBounds=YES;
        hexValueBackgroundView.layer.cornerRadius=hexValueBackgroundView.frame.size.height/2;
        
        CGRect hexLableFrame1=CGRectMake(10, 5, 100, 15);
        UILabel *hexLable1=[[UILabel alloc]initWithFrame:hexLableFrame1];
        [hexLable1 setText:@"Hex"];
        [hexLable1 setTextAlignment:NSTextAlignmentCenter];
        [hexLable1 setTextColor:[UIColor whiteColor]];
        [hexLable1 setBackgroundColor:[UIColor clearColor]];
        hexLable1.layer.cornerRadius=6.0;
        hexLable1.clipsToBounds=YES;
        
        
        CGRect hexLableFrame2=CGRectMake(10, 25, 100, 15);
        hexLable2=[[UILabel alloc]initWithFrame:hexLableFrame2];
        [hexLable2 setText:@"Hex Value"];
        [hexLable2 setTextAlignment:NSTextAlignmentCenter];
        [hexLable2 setTextColor:[UIColor whiteColor]];
        [hexLable2 setBackgroundColor:[UIColor clearColor]];
        hexLable2.layer.cornerRadius=6.0;
        hexLable2.clipsToBounds=YES;
        
        
        [hexValueBackgroundView addSubview:hexLable1];
        [hexValueBackgroundView addSubview:hexLable2];
        [hexView addSubview:hexValueBackgroundView];
        [self.hexRgbColorView addSubview:hexView];
        
        UITapGestureRecognizer *tapGR1;
        tapGR1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapHex:)];
        tapGR1.numberOfTapsRequired = 1;
        [hexView addGestureRecognizer:tapGR1];
        
        
        rgbView=[[UIView alloc]initWithFrame:CGRectMake(160, 15, 145, 70)];
        rgbView.backgroundColor=[UIColor grayColor];
        rgbView.layer.masksToBounds=YES;
        rgbView.layer.cornerRadius=hexView.frame.size.height/2;
        
        UIView *rgbValueBackgroundView=[[UIView alloc]initWithFrame:CGRectMake(12, 10, 120, 50)];
        rgbValueBackgroundView.backgroundColor=[[UIColor grayColor] colorWithAlphaComponent:0.5];
        rgbValueBackgroundView.layer.masksToBounds=YES;
        rgbValueBackgroundView.layer.cornerRadius=rgbValueBackgroundView.frame.size.height/2;
        
        CGRect rgbLableFrame1=CGRectMake(10, 5, 100, 15);
        UILabel *rgbLable1=[[UILabel alloc]initWithFrame:rgbLableFrame1];
        [rgbLable1 setText:@"RGB"];
        [rgbLable1 setTextAlignment:NSTextAlignmentCenter];
        [rgbLable1 setTextColor:[UIColor whiteColor]];
        [rgbLable1 setBackgroundColor:[UIColor clearColor]];
        rgbLable1.layer.cornerRadius=6.0;
        rgbLable1.clipsToBounds=YES;
        
        CGRect rgbLableFrame2=CGRectMake(10, 25, 110, 15);
        rgbLable2=[[UILabel alloc]initWithFrame:rgbLableFrame2];
        [rgbLable2 setText:@"RGB Value"];
        [rgbLable2 setTextAlignment:NSTextAlignmentCenter];
        [rgbLable2 setTextColor:[UIColor whiteColor]];
        [rgbLable2 setBackgroundColor:[UIColor clearColor]];
        rgbLable2.layer.cornerRadius=6.0;
        rgbLable2.clipsToBounds=YES;
        
        [rgbValueBackgroundView addSubview:rgbLable1];
        [rgbValueBackgroundView addSubview:rgbLable2];
        [rgbView addSubview:rgbValueBackgroundView];
        [self.hexRgbColorView addSubview:rgbView];
        
        UITapGestureRecognizer *tapGR2;
        tapGR2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapRgb:)];
        tapGR2.numberOfTapsRequired = 1;
        [rgbView addGestureRecognizer:tapGR2];
        
    }

    self->izValueSelecter.dataSource = self;
    self->izValueSelecter.delegate = self;
    self->izValueSelecter.shouldBeTransparent = YES;
    self->izValueSelecter.horizontalScrolling = YES;
    self->izValueSelecter.decelerates=NO;
    self->colorPickerView.color =_getColor;
    
}

- (void)colorWasChanged:(HRColorPickerView *)colorPickerView {
    
    mainColor=self->colorPickerView.color;
    lightColor1=[self lighterColorForColor:mainColor];
    lightColor2=[self lighterColorForColor:lightColor1];
    lightColor3=[self lighterColorForColor:lightColor2];
    lightColor4=[self lighterColorForColor:lightColor3];
    
    darkColor1=[self darkerColorForColor:mainColor];
    darkColor2=[self darkerColorForColor:darkColor1];
    darkColor3=[self darkerColorForColor:darkColor2];
    darkColor4=[self darkerColorForColor:darkColor3];
    darkColor5=[self darkerColorForColor:darkColor4];
    
    hexView.backgroundColor = self->colorPickerView.color;
    rgbView.backgroundColor=self->colorPickerView.color;
    hexLable2.text=[self hexStringFromColor:self->colorPickerView.color];
    rgbLable2.text=[self rgbValueWithHexString:hexLable2.text];
    
    [self->izValueSelecter reloadData];
    [self->izValueSelecter selectRowAtIndex:4 animated:YES];
    NSLog(@"reload table");
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    izValueSelecter.frame=(CGRect){ .origin = CGPointZero, .size = self.valueSelecterView.frame.size};
    colorPickerView.frame = (CGRect) {.origin = CGPointZero, .size = self.pickerView.frame.size};
//    colorPickerView.frame=CGRectMake(0,0, self.pickerView.frame.size.width, self.pickerView.frame.size.height);
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    mainColor=self->colorPickerView.color;
    lightColor1=[self lighterColorForColor:mainColor];
    lightColor2=[self lighterColorForColor:lightColor1];
    lightColor3=[self lighterColorForColor:lightColor2];
    lightColor4=[self lighterColorForColor:lightColor3];
    
    darkColor1=[self darkerColorForColor:mainColor];
    darkColor2=[self darkerColorForColor:darkColor1];
    darkColor3=[self darkerColorForColor:darkColor2];
    darkColor4=[self darkerColorForColor:darkColor3];
    darkColor5=[self darkerColorForColor:darkColor4];
    
    hexView.backgroundColor=self->colorPickerView.color;
    rgbView.backgroundColor=self->colorPickerView.color;
    hexLable2.text=[self hexStringFromColor:self->colorPickerView.color];
    rgbLable2.text=[self rgbValueWithHexString:hexLable2.text];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
        [self->izValueSelecter reloadData];
        [self->izValueSelecter selectRowAtIndex:4 animated:YES];
}

#pragma IZValueSelector dataSource
- (NSInteger)numberOfRowsInSelector:(IZValueSelectorView *)valueSelector {
    return 9;
}
- (CGFloat)rowHeightInSelector:(IZValueSelectorView *)valueSelector {
    return 50.0;
}
- (CGFloat)rowWidthInSelector:(IZValueSelectorView *)valueSelector {
    return 40.0;
}
- (UIView *)selector:(IZValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index
{
    return [self selector:valueSelector viewForRowAtIndex:index selected:NO];
}

- (UIView *)selector:(IZValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index selected:(BOOL)selected
{
    UIView * label = nil;
   
    label = [[UIView alloc] initWithFrame:CGRectMake(0, 0,40, 50)];
    label.layer.borderWidth=1.0f;
    label.layer.borderColor=[UIColor blackColor].CGColor;

    if(index == 0)
    {
        label.backgroundColor=lightColor4;
        finalColor=lightColor4;
    }
    if(index == 1)
    {
        
        label.backgroundColor=lightColor3;
        finalColor=lightColor3;
    }
    if(index == 2)
    {
        
        label.backgroundColor=lightColor2;
        finalColor=lightColor2;
    }
    if(index == 3)
    {
        
        label.backgroundColor=lightColor1;
        finalColor=lightColor1;
    }
    if(index == 4)
    {
        
        label.backgroundColor=mainColor;
        finalColor=mainColor;
    }
    if(index == 5)
    {
        
        label.backgroundColor=darkColor1;
        finalColor=darkColor1;
    }
    if(index == 6)
    {
        
        label.backgroundColor=darkColor2;
        finalColor=darkColor2;
    }
    if(index == 7)
    {
        
        label.backgroundColor=darkColor3;
        finalColor=darkColor3;
    }
    if(index == 8)
    {
        
        label.backgroundColor=darkColor4;
        finalColor=darkColor4;
    }
    if(selected)
    {
        NSLog(@"get Index: %ld",index);
        
        hexLable2.text=[self hexStringFromColor:self->finalColor];
        rgbLable2.text=[self rgbValueWithHexString:hexLable2.text];
        hexView.backgroundColor = finalColor;
        rgbView.backgroundColor = finalColor;
        self.tringleView.backgroundColor=finalColor;
        sendColor=finalColor;
        pickColor=finalColor;
    }
   
    
    return label;
}

- (CGRect)rectForSelectionInSelector:(IZValueSelectorView *)valueSelector
{
    return CGRectMake(valueSelector.frame.size.width/2, valueSelector.frame.size.height/2, 1, 1);
}

#pragma IZValueSelector delegate
- (void)selector:(IZValueSelectorView *)valueSelector didSelectRowAtIndex:(NSInteger)index {
    NSLog(@"Home IZ Selected index %ld",(long)index);
    
    
}

//Return RGB value string
-(NSString* )rgbValueWithHexString:(NSString*)string
{
    NSString *stringColor = string;
    NSUInteger red, green, blue;
    sscanf([stringColor UTF8String], "#%2lX%2lX%2lX", &red, &green, &blue);
    return [NSString stringWithFormat:@"%lu,%lu,%lu",(unsigned long)red,(unsigned long)green,(unsigned long)blue];
}
//Return Hex value string
- (NSString *)hexStringFromColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    
    return [NSString stringWithFormat:@"#%02lX%02lX%02lX",
            lroundf(r * 255),
            lroundf(g * 255),
            lroundf(b * 255)];
}
- (UIColor *)lighterColorForColor:(UIColor *)c
{
    CGFloat r, g, b, a;
    if ([c getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MIN(r + 0.2, 1.0)
                               green:MIN(g + 0.2, 1.0)
                                blue:MIN(b + 0.2, 1.0)
                               alpha:a];
    return nil;
}

- (UIColor *)darkerColorForColor:(UIColor *)c
{
    CGFloat r, g, b, a;
    if ([c getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MAX(r - 0.2, 0.0)
                               green:MAX(g - 0.2, 0.0)
                                blue:MAX(b - 0.2, 0.0)
                               alpha:a];
    return nil;
}

// Add a delegate method to handle the tap
-(void)handleTapHex:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self performSegueWithIdentifier:@"HexValuesViewController" sender:nil];
    }
}
-(void)handleTapRgb:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded) {
       [self performSegueWithIdentifier:@"RGBValuesViewController" sender:nil];
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    if ([segue.destinationViewController isKindOfClass:[HexValuesViewController class]]) {
        HexValuesViewController *controller = segue.destinationViewController;
        controller.delegate = (id)self;
        controller.getColor=sendColor;
    }
    if([segue.destinationViewController isKindOfClass:[RGBValuesViewController class]])
    {
        RGBValuesViewController *controller=segue.destinationViewController;
        controller.delegate=(id)self;
        controller.getColor=sendColor;
        
    }

}
- (void)setSelectedColor:(UIColor *)color {
    self->colorPickerView.color=color;
}
- (IBAction)buttonDoneAction:(id)sender
{
    if(self.pickerDelegate)
    {
        [self.pickerDelegate setSelectedColor:pickColor];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
