//
//  RGBValuesViewController.h
//  ColorPicker Project
//
//  Created by Iftekhar on 15/01/16.
//  Copyright © 2016 iftekhar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RGBValuesViewControllerDelegate
- (void)setSelectedColor:(UIColor *)color;
@end

@interface RGBValuesViewController : UIViewController

@property (strong,nonatomic)  UIColor *getColor;
@property (nonatomic, strong) UIColor* color;
@property (nonatomic, weak) id <RGBValuesViewControllerDelegate> delegate;


@end
