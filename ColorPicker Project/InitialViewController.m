//
//  InitialViewController.m
//  ColorPicker Project
//
//  Created by Iftekhar on 27/01/16.
//  Copyright © 2016 iftekhar. All rights reserved.
//

#import "InitialViewController.h"
#import "ColorPickerViewController.h"

@interface InitialViewController ()
@property (strong, nonatomic) IBOutlet UIView *pickColorView;
@property (strong, nonatomic) IBOutlet UIButton *pickeColorButton;
@property (strong, nonatomic) IBOutlet UIView *backGroundView;

@end

@implementation InitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    self.pickColorView.backgroundColor=color;
   
    // Do any additional setup after loading the view.
    self.pickColorView.layer.borderWidth=1.0f;
    self.pickColorView.layer.borderColor=[UIColor blackColor].CGColor;
    self.pickeColorButton.clipsToBounds=YES;
   _backGroundView.layer.masksToBounds=YES;
   

    
}
-(void)viewDidLayoutSubviews
{
     _backGroundView.layer.cornerRadius=_backGroundView.frame.size.height/2;
     self.pickeColorButton.layer.cornerRadius=self.pickeColorButton.frame.size.height/2;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.pickeColorButton.backgroundColor=[self.pickColorView.backgroundColor colorWithAlphaComponent:0.5];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)pickColorButton:(id)sender
{
    [self performSegueWithIdentifier:@"ColorPickerViewController" sender:nil];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    if([segue.identifier isEqualToString:@"ColorPickerViewController"])
    {
        UINavigationController *navi=[segue destinationViewController];
        ColorPickerViewController *colorPickerVC = (ColorPickerViewController*)navi.topViewController;
        colorPickerVC.pickerDelegate=(id)self;
        colorPickerVC.getColor=self.pickColorView.backgroundColor;
    }
}
-(void)setSelectedColor:(UIColor*)color
{
    self.pickColorView.backgroundColor=color;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
