//
//  HexValuesViewController.m
//  ColorPicker Project
//
//  Created by Iftekhar on 15/01/16.
//  Copyright © 2016 iftekhar. All rights reserved.
//

#import "HexValuesViewController.h"

@interface HexValuesViewController ()<UIPickerViewDataSource,UIPickerViewDelegate>
{
    
}
@property (strong, nonatomic) IBOutlet UIView *labelView;
@property (strong, nonatomic) IBOutlet UIView *colorView;
@property (strong, nonatomic) IBOutlet UILabel *pickerLabel;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (strong, nonatomic) IBOutlet UILabel *pickerLabelRGB;
@property (strong, nonatomic)          NSArray *pickerValueArray;



@end

@implementation HexValuesViewController
@synthesize delegate;
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.colorView.layer.borderColor = [UIColor blackColor].CGColor;
    self.colorView.layer.borderWidth = 1.0f;
    
    self.pickerLabel.layer.cornerRadius=6.0;
    self.pickerLabel.clipsToBounds=YES;
    self.pickerLabelRGB.layer.cornerRadius=6.0;
    self.pickerLabelRGB.clipsToBounds=YES;
    
    //self.labelView.layer.cornerRadius=25;
    self.labelView.layer.cornerRadius=self.labelView.frame.size.height/2;
    self.labelView.layer.masksToBounds=YES;
    
    self.pickerValueArray  = [[NSArray alloc]initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5" ,@"6",@"7",@"8",@"9",@"A",@"B" ,@"C",@"D",@"E",@"F", nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _pickerLabel.text=[self hexStringFromColor:_getColor];
    _pickerLabelRGB.text=[self rgbValueWithHexString:_pickerLabel.text];
    _colorView.backgroundColor=_getColor;
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSString * char1 = [_pickerLabel.text substringWithRange:NSMakeRange(1, 1)];
    NSString *char2=[_pickerLabel.text substringWithRange:NSMakeRange(2, 1)];
    NSString *char3=[_pickerLabel.text substringWithRange:NSMakeRange(3, 1)];
    NSString *char4=[_pickerLabel.text substringWithRange:NSMakeRange(4, 1)];
    NSString *char5=[_pickerLabel.text substringWithRange:NSMakeRange(5, 1)];
    NSString *char6=[_pickerLabel.text substringWithRange:NSMakeRange(6, 1)];
    
    NSUInteger selectedRow1 = [self.pickerValueArray indexOfObject: char1];
    NSUInteger selectedRow2 = [self.pickerValueArray indexOfObject: char2];
    NSUInteger selectedRow3 = [self.pickerValueArray indexOfObject: char3];
    NSUInteger selectedRow4 = [self.pickerValueArray indexOfObject: char4];
    NSUInteger selectedRow5 = [self.pickerValueArray indexOfObject: char5];
    NSUInteger selectedRow6 = [self.pickerValueArray indexOfObject: char6];
    [_picker selectRow:selectedRow1 inComponent:0 animated:YES];
    [_picker reloadComponent:0];
    [_picker selectRow:selectedRow2 inComponent:1 animated:YES];
    [_picker reloadComponent:1];
    [_picker selectRow:selectedRow3 inComponent:2 animated:YES];
    [_picker reloadComponent:2];
    [_picker selectRow:selectedRow4 inComponent:3 animated:YES];
    [_picker reloadComponent:3];
    [_picker selectRow:selectedRow5 inComponent:4 animated:YES];
    [_picker reloadComponent:4];
    [_picker selectRow:selectedRow6 inComponent:5 animated:YES];
    [_picker reloadComponent:5];

}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.labelView.layer.cornerRadius=self.labelView.frame.size.height/2;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 6;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return 16;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    
    return [self.pickerValueArray objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component __TVOS_PROHIBITED
{
    
    NSString *component1=[self.pickerValueArray objectAtIndex:[_picker selectedRowInComponent:0]];
    NSString*component2=[self.pickerValueArray objectAtIndex:[_picker selectedRowInComponent:1]];
    NSString*component3=[self.pickerValueArray objectAtIndex:[_picker selectedRowInComponent:2]];
    NSString*component4=[self.pickerValueArray objectAtIndex:[_picker selectedRowInComponent:3]];
    NSString*component5=[self.pickerValueArray objectAtIndex:[_picker selectedRowInComponent:4]];
    NSString*component6=[self.pickerValueArray objectAtIndex:[_picker selectedRowInComponent:5]];
    
    _pickerLabel.text=[NSString stringWithFormat:@"#%@%@%@%@%@%@",component1,component2,component3,component4,component5,component6];
    NSString *stringColor = _pickerLabel.text;
    NSUInteger red, green, blue;
    sscanf([stringColor UTF8String], "#%2lX%2lX%2lX", &red, &green, &blue);
   
    UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1];
    _colorView.backgroundColor=color;
    //set Color to color property
    self.color=color;
    self.pickerLabelRGB.text=[NSString stringWithFormat:@"(%lu,%lu,%lu)",(unsigned long)red,(unsigned long)green,(unsigned long)blue];
    
}
//Return RGB value string
-(NSString* )rgbValueWithHexString:(NSString*)string
{
    NSString *stringColor = string;
    NSUInteger red, green, blue;
    sscanf([stringColor UTF8String], "#%2lX%2lX%2lX", &red, &green, &blue);
    return [NSString stringWithFormat:@"%lu,%lu,%lu",(unsigned long)red,(unsigned long)green,(unsigned long)blue];
}
- (NSString *)hexStringFromColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    
    return [NSString stringWithFormat:@"#%02lX%02lX%02lX",
            lroundf(r * 255),
            lroundf(g * 255),
            lroundf(b * 255)];
}
- (IBAction)buttonDoneAction:(id)sender
{
    if (self.delegate) {
        [self.delegate setSelectedColor:self.color];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
@end
